import axios from "axios";

const createClient = (baseURL: string) => {
  const instance = axios.create({
    baseURL,
    responseType: "json",
    timeout: 15000,
    validateStatus: function (status) {
      return status >= 200 && status <= 302;
    },
  });
  return instance;
};

export const api = createClient("http://localhost:3000/api/v1/");
api.defaults.headers.common.Accept = "application/json";
api.defaults.headers.post["Content-Type"] = "application/json";
