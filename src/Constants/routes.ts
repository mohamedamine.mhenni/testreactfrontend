import IRoute from "../Interfaces/route.interface";
import HomePage from "../Pages/Home/Home";
import TodoPage from "../Pages/Todos/Todo";

const routes: IRoute[] = [
  {
    path: "/",
    name: "Home Page",
    exact: true,
    component: HomePage,
  },
  {
    path: "/todos/:id",
    name: "Todos Page",
    exact: true,
    component: TodoPage,
  },
];
export default routes;
