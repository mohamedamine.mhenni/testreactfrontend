import "./App.css";
import {
  BrowserRouter,
  Route,
  Switch,
  Redirect,
  RouteComponentProps,
} from "react-router-dom";
import routes from "./Constants/routes";
import Layout from "./Components/Layout/Layout";
function App() {
  return (
    <BrowserRouter>
      <Layout>
        <Switch>
          {routes.map((route, index) => (
            <Route
              key={index}
              path={route.path}
              exact={route.exact}
              render={(props: RouteComponentProps<any>) => (
                <route.component
                  name={route.name}
                  {...props}
                  {...route.props}
                />
              )}
            />
          ))}
          <Redirect to="/error" />
        </Switch>
      </Layout>
    </BrowserRouter>
  );
}

export default App;
