export default interface ITodo {
  title: string;
  owner: string;
  description: string;
}
