export default interface IUser {
  _id: string;
  firstName: string;
  readonly lastName: string;
  readonly email: string;
  readonly phone: string;
  readonly address: string;
  readonly description: string;
}
