import { Container, Grid, Paper } from "@mui/material";
import React, { FC, useCallback, useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import SearchBar from "../../Components/SearchBar/SearchBar";
import ITodo from "../../Interfaces/todo.interface";
import TodoService from "../../Services/TodoService";
import IPage from "../../Interfaces/page.interface";

const Todo: FC<IPage> = () => {
  const { id } = useParams() as {
    id: string;
  };
  const [todos, setTodos] = useState<Array<ITodo>>([]);
  const [loading, setLoading] = useState<boolean>(true);
  const [error, setError] = useState<boolean>(false);
  const [searchTxt, setSearchTxt] = useState<string>("");

  const handleSearchTextChange = useCallback(
    (text: string) => {
      console.log(text);
      setSearchTxt(text);
    },
    [searchTxt]
  );

  const fetchUserTodos = async (searchTxt: string) => {
    try {
      const { data } = await TodoService.getAll(id, searchTxt);
      setTodos(data);
      setLoading(false);
      console.log("TODOS :: ", data);
    } catch (e) {
      setError(true);
    }
  };

  useEffect(() => {
    fetchUserTodos(searchTxt);
  }, [searchTxt]);

  return loading ? (
    <div>Loading ...</div>
  ) : error ? (
    <div>Error ...</div>
  ) : (
    <Container maxWidth="lg">
      <SearchBar
        title="Todo List"
        handleSearchTextChange={handleSearchTextChange}
      />
      <Grid container spacing={2}>
        {todos.length > 0 ? (
          todos.map((todo: ITodo, index: number) => (
            <Grid item xs={6} key={index}>
              <Paper
                style={{ textAlign: "center", padding: 20 }}
                variant="outlined"
              >
                {todo.title}
              </Paper>
            </Grid>
          ))
        ) : (
          <div>THERE IS NO TODO</div>
        )}
      </Grid>
    </Container>
  );
};
export default Todo;
