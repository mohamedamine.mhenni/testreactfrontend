import { Container, Grid } from "@mui/material";
import React, { FC, useEffect, useState, useCallback } from "react";
import Card from "../../Components/Card/Card";
import TablePagination from "@mui/material/TablePagination";
import IUser from "../../Interfaces/user.interface";
import UserService from "../../Services/UserService";
import SearchBar from "../../Components/SearchBar/SearchBar";
import IPage from "../../Interfaces/page.interface";
const Home: FC<IPage> = () => {
  const [users, setUsers] = useState<Array<IUser>>([]);
  const [loading, setLoading] = useState<boolean>(true);
  const [error, setError] = useState<boolean>(false);
  const [searchTxt, setSearchTxt] = useState<string>("");
  const [page, setPage] = useState<number>(0);
  const [rowsPerPage, setRowsPerPage] = useState<number>(10);
  const [totalCount, setTotalCount] = useState<number>(0);

  const fetchUser = async (
    searchTxt: string,
    limit: number,
    offset: number
  ) => {
    try {
      const { data } = await UserService.getAll(searchTxt, limit, offset);
      console.log("FETCHED USER :: ", data);
      setUsers(data.users);
      setPage(data.offset);
      setRowsPerPage(data.limit);
      setTotalCount(data.totalCount);
      setLoading(false);
    } catch (e) {
      setError(true);
    }
  };

  // Dans cette fonction de préférence qu'on utilise le debouncing pour éviter le bombardement de serveur back
  const handleSearchTextChange = useCallback(
    (text: string) => {
      console.log(text);
      setSearchTxt(text);
    },
    [searchTxt]
  );

  const handleChangePage = (
    event: React.MouseEvent<HTMLButtonElement> | null,
    newPage: number
  ) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (
    event: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>
  ) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };
  useEffect(() => {
    fetchUser(searchTxt, rowsPerPage, page);
  }, [searchTxt]);
  return loading ? (
    <div>Loading ...</div>
  ) : error ? (
    <div>Error ...</div>
  ) : (
    <Container maxWidth="lg">
      <SearchBar
        title="Users List"
        handleSearchTextChange={handleSearchTextChange}
      />
      <Grid container spacing={2}>
        {users.map((user: IUser, index: number) => (
          <Grid key={index} item xs={12} md={6}>
            <Card user={user} />
          </Grid>
        ))}
      </Grid>
      <TablePagination
        component="div"
        count={totalCount}
        page={page}
        onPageChange={handleChangePage}
        rowsPerPage={rowsPerPage}
        onRowsPerPageChange={handleChangeRowsPerPage}
      />
    </Container>
  );
};
export default Home;
