import React, { FC, ReactNode } from "react";
import { IconButton, InputBase } from "@mui/material";
import SearchIcon from "@mui/icons-material/Search";

interface ISearchBar {
  title: string;
  handleSearchTextChange: any;
}

const SearchBar: FC<ISearchBar> = ({
  title,
  handleSearchTextChange,
}: ISearchBar) => {
  return (
    <div className="header_container">
      <div className="headerTitle">{title}</div>
      <div className="filterContainer">
        <InputBase
          sx={{ flex: 1 }}
          onChange={(e) => handleSearchTextChange(e.target.value)}
          placeholder=" Search Todo by title"
          inputProps={{ "aria-label": "search google maps" }}
        />
        <IconButton type="submit" sx={{ p: "10px" }} aria-label="search">
          <SearchIcon />
        </IconButton>
      </div>
    </div>
  );
};

export default SearchBar;
