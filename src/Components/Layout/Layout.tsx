import React, { FC, ReactNode } from "react";
import IUser from "../../Interfaces/user.interface";
import { Link } from "react-router-dom";
interface IRouteChildrens {
  children: ReactNode;
}

const Layout: FC<IRouteChildrens> = ({ children }: IRouteChildrens) => {
  return (
    <div>
      <div>
        <main>{children}</main>
      </div>
    </div>
  );
};

export default Layout;
