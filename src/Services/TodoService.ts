import { api } from "../Config/AxiosConfig";
const getAll = async (ownerId: string, searchTxt: string) => {
  const { data } = await api.post(
    "todo/getMyTodos",
    { userId: ownerId },
    { params: { searchTxt: searchTxt } }
  );
  return data;
};

const TodoService = {
  getAll,
};
export default TodoService;
