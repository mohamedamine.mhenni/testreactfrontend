import { api } from "../Config/AxiosConfig";
const getAll = async (searchTxt: string, limit: number, offset: number) => {
  const { data } = await api.get("users", {
    params: { searchTxt: searchTxt, limit: limit, offset: offset },
  });
  return data;
};

const UserService = {
  getAll,
};
export default UserService;
